import React from 'react';
import { IonRow, IonCol, IonItemDivider } from '@ionic/react';
import Details from './Details';

const Pose = ({ decades, f1 }) => {
    return <>
        {decades[0] * f1}
        {decades.slice(1).map(element =>
            <><br /> + {element * f1}</>
        )}
    </>
}


const PoserMulti = ({ f1, f2 }) => {

    var decades = [];
    if (f1 && f2) {
        const str = f2.toString();
        for (let i = str.length - 1; i >= 0; i--) {
            const element = parseInt(str[i]) * (10 ** (str.length - i - 1));
            decades.push(element);
        }
    }

    return (


        <IonRow>
            <IonCol size="6">
                {(f1 && f2) ? (<div id="multi">
                    <div className='hr'>{f1}<br /> x {f2}<br /></div>
                    <div className='hr'><Pose decades={decades} f1={f1} /> <br /></div>
                    <div>{f1 * f2}</div>
                </div>) : (null)
                }
            </IonCol>
            <IonCol size="6">
                <br /><br />
                <Details f1={f1} decades={decades} />
            </IonCol>
        </IonRow >)
}

export default PoserMulti;