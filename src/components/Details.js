import React from 'react';

const Detail = ({ a, b }) => {
    return <>
        = {a} x {b}
    </>
}
const Details = ({ f1, decades }) => {
    return <div id="details">

        {decades.map(element =>
            <><br /><Detail a={element} b={f1} /></>
        )}

    </div>
}
export default Details;