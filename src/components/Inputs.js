import React from "react"
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { IonGrid, IonRow, IonCol } from '@ionic/react';
import PoserMulti from './PoserMulti';



const Inputs = () => {
    const [facteur1, setFacteur1] = React.useState(undefined);
    const [facteur2, setFacteur2] = React.useState(undefined);

    /*<  button onClick={poserMulti(facteur1, facteur2)}>Go !</button>*/

    return (
        <IonGrid id="grid">
            <IonRow>
                <IonCol id="col"><div>
                    <input
                        type="number"
                        placeholder="Facteur1"
                        STYLE="Text-ALIGN:center"
                        onChange={(event) => setFacteur1(event.target.value)}
                    ></input>
                    <br />
        x
        <br />
                    <input
                        type="number"
                        placeholder="Facteur2"
                        STYLE="Text-ALIGN:center"
                        onChange={(event) => setFacteur2(event.target.value)}
                    ></input>
                    <br />
                    <hr />

                </div></IonCol>

            </IonRow>
            <IonRow>
                <IonCol><PoserMulti f1={facteur1} f2={facteur2} /></IonCol>
            </IonRow>
        </IonGrid>
    )
}

export default Inputs